# Darien Pittman

## I'm a Junior Computer Science Major Mathematics Minor at Morehouse College.

My goal is to be able to combine my studies with my passions to make a career I love!

## Hobbies and interest

Outside of the classroom I am on the Morehouse Football Team, where I am the starting Cornerback. I enjoy being active and working out, I enjoy going out around the city with my friends, and I also enjoy videogames as well.

## Experience
Languages:
- C++
- Python
Orgnizations:
-SAAC (Student Athlete Advisory Committee)

